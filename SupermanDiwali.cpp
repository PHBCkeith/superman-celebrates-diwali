/**
*
* My solution to the Superman Celebrates Diwali challenge on hackerrank.com
*
* Copyright (C) 2018 by Keith Jordan <3keithjordan3@gmail.com>
*
* This program is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
* 
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
* 
* You should have received a copy of the GNU General Public License
* along with this program.  If not, see <http://www.gnu.org/licenses/>
* 
**/

#include <stdio.h>
#include <vector>

using namespace std;

// optional-like template class.  It allows for determinination of whether or not the value
// has been set yet.  The bool operator provides a way to check this.  Once set, it cannot
// be unset.
template <class T>
class MyOptional {

	T value;
	bool isSet;

public:

	MyOptional() {
		isSet = false;
	}

	void setValue(T t) {
		value = t;
		isSet = true;
	}

	long getValue() {
		return value;
	}

	operator bool() {
		return (isSet);
	}

	void operator=(T otherT) {
		setValue(otherT);
	}

};

// 2D vector that holds subsolutions for a particular [building] and [floor]
vector<vector<MyOptional<long>>> subSolutions;
// vector that records best jumping sub solution from a given floor
vector<MyOptional<long>> bestJump;

// recursive portion of algorithm
long getSavedPeopleFromBuildingFloor(int building, int floor, vector<vector<int>> &buildingFloorPops,
																		int jumpHeight) {

	// if there is already a subsolution, use it
	if (subSolutions[building][floor])
		return subSolutions[building][floor].getValue();

	// if we're at the ground floor, save the people there and exit
	if (floor == 0)
		return buildingFloorPops[building][floor];

	// get saved people if superman just goes downstairs
	long nextSavedPeople = getSavedPeopleFromBuildingFloor(building, floor - 1,	
																									      buildingFloorPops, jumpHeight);
	// if there isn't a memoized best jumping subsolution, find it
	if (!bestJump[floor]) {
		// if the current floor is high enough for superman to jump to another building
		if (floor >= jumpHeight) {
			bestJump[floor] = 0;
			for (int i = 0; (i < buildingFloorPops.size()); ++i) {
				// get saved people if superman jumps to building i
				long savedPeople = getSavedPeopleFromBuildingFloor(i, floor - jumpHeight,
																													buildingFloorPops, jumpHeight);
				if (savedPeople > bestJump[floor].getValue()) {
					bestJump[floor] = savedPeople;
				}
			}
			if (bestJump[floor].getValue() > nextSavedPeople) {
				nextSavedPeople = bestJump[floor].getValue();
			}
		}
	} else {
		// jump if jumping is better than going down the stairs
		if (bestJump[floor].getValue() > nextSavedPeople)
			nextSavedPeople = bestJump[floor].getValue();
	}

	// memoize subsolution
	subSolutions[building][floor] = nextSavedPeople + buildingFloorPops[building][floor];

	return subSolutions[building][floor].getValue();

}

// wrapper function for recursive algorithm.  Checks solution with superman starting
// from each building top.
long getMaxSavedPeople(vector<vector<int>> &buildingFloorPops, int jumpHeight) {

	long maxSavedPeople = 0;
	// check from each building top
	for (int i = 0; i < buildingFloorPops.size(); ++i) {
		long savedPeople = getSavedPeopleFromBuildingFloor(i, buildingFloorPops[0].size() - 1,
																											buildingFloorPops, jumpHeight);
		if (savedPeople > maxSavedPeople)
			maxSavedPeople = savedPeople;
	}

	return maxSavedPeople;

}

int main() {

	int numBuildings, buildingHeight, jumpHeight;

	scanf("%d %d %d", &numBuildings, &buildingHeight, &jumpHeight);

	// 2D vector.  Each value is the number of people for a particular building and floor
	vector<vector<int>> buildingFloorPops(numBuildings, vector<int>(buildingHeight, 0));

	// initialize memoization vectors
	subSolutions = vector<vector<MyOptional<long>>>(numBuildings,
								 vector<MyOptional<long>>(buildingHeight, MyOptional<long>()));
	bestJump = vector<MyOptional<long>>(buildingHeight, MyOptional<long>());

	// get input
	for (int i = 0; i < numBuildings; ++i) {
		int numPeople;
		scanf("%d", &numPeople);
		for (int j = 0; j < numPeople; ++j) {
			int floor;
			scanf("%d", &floor);
			buildingFloorPops[i][floor - 1]++;
		}
	}

	// find and output solution
	printf("%ld\n", getMaxSavedPeople(buildingFloorPops, jumpHeight));

	return 0;
}